﻿using ChameleonQuestionsAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Data
{

    /// <summary>
    /// This class seeds data into the database at context build.
    /// </summary>
    public static class ModelBuilderExtensions
    {


        public static void Seed(this ModelBuilder modelBuilder)
        {
            for (int i = 1; i < 1001; i++)
            {


                modelBuilder.Entity<Question>().HasData(
                new Question
                {
                    Id = i,
                    Category = "Category " + i
                }
                );

                for (int j = 1; j < 11; j++)
                {
                    modelBuilder.Entity<Alternative>().HasData(
                        new Alternative
                        {
                            Id = i*10 + j,
                            AlternativeString = "Alternative " + j + " Cat " + i,
                            QuestionId = i,
                        }
                        ) ;
                }
            }

        }


    }
}
