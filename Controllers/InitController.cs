﻿using ChameleonQuestionsAPI.Data;
using ChameleonQuestionsAPI.Hubs;
using ChameleonQuestionsAPI.Hubs.Clients;
using ChameleonQuestionsAPI.Models;
using ChameleonQuestionsAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;


namespace ChameleonQuestionsAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class InitController : ControllerBase
    {
        private readonly ChameleonQuestionAPIdb _context;
        private readonly IHubContext<GameHub, IGameClient> _hub;
        private readonly IGamePinService _gamePinService;
        public InitController(
            ChameleonQuestionAPIdb context, 
            IHubContext<GameHub, IGameClient> hub,
            IGamePinService gamePinService
            )
        {
            _context = context;
            _hub = hub;
            _gamePinService = gamePinService;
        }

        /// <summary>
        /// Initializes and empty game in the DB. Very susceptible to ddos attacks
        /// </summary>
        /// <returns></returns>
        [HttpGet("newgame")]
        public async Task<ActionResult<int>> InitializeGetEmptyGamePin()
        {

            return _gamePinService.NextGamePin();
        }

        [HttpGet("questions")]
        public async Task<ActionResult<int>> GetQuestionsCount() 
        {
            var questions = await _context.Questions.ToListAsync();

            return questions.Count;
        }



    } 
}

