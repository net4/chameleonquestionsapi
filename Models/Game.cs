﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Models
{
    public class Game
    {
        [Key]
        public string GamePin { get; set; }
        public ICollection<Player> Players { get; set; } = new List<Player>();


    }
}
