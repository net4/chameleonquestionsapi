﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Models
{
    public class Player
    {

        public string Id { get; set; }

        public string GamePin { get; set; }

        public bool IsHost { get; set; } = false;

        [Required]
        public string Nickname { get; set; }
    }
}
