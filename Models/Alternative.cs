﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Models
{
    public class Alternative
    {
        public int Id { get; set; }
        public string AlternativeString { get; set; }
        public Question Question { get;  set; }
        public int QuestionId { get; internal set; }
    }
}
