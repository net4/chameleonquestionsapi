﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Models
{
    public class Question
    {
        //Primary Key
        [Key]
        public int Id { get; set; }

        [Required]
        public string Category { get; set; }

        [Required]
        public ICollection<Alternative> Alternatives { get; set; }
    }
}
