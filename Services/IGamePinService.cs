﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Services
{
    public interface IGamePinService
    {
        int NextGamePin();
    }
}
