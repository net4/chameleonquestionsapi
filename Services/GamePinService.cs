﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Services
{
    /// <summary>
    /// Dependency injected class to keep track of GamePins
    /// </summary>
    public class GamePinService : IGamePinService
    {
        private int GamePin { get; set; }

        public GamePinService() {
            GamePin = 0;
        }

        public int NextGamePin() {
            if (GamePin >= 100000)
            {
                GamePin = 0;
            }
            else 
            {
                GamePin++;
            }
            return GamePin;
        }
    }
}
