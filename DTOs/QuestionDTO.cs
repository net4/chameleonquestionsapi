﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.DTOs
{
    public class QuestionDTO
    {
        public string Category { get; set; }
        public List<string> Alternatives { get; set; }

    }
}
