﻿using AutoMapper;
using ChameleonQuestionsAPI.Data;
using ChameleonQuestionsAPI.DTOs;
using ChameleonQuestionsAPI.Hubs.Clients;
using ChameleonQuestionsAPI.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Hubs
{
    public class GameHub : Hub<IGameClient>
    {
        private readonly ChameleonQuestionAPIdb _context;
        private readonly Random Random = new Random();
        private readonly IMapper _mapper;
        public GameHub(ChameleonQuestionAPIdb context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task JoinRoom(Player player)
        {
            player.Id = Context.ConnectionId;
            await Groups.AddToGroupAsync(Context.ConnectionId, player.GamePin.ToString());
            await Clients.Group(player.GamePin.ToString()).JoinGame(player);

        }


        /// <summary>
        /// Gets an updated lobby of players and sends it through hub to all clients in group
        /// </summary>
        public async Task UpdateLobby(List<Player> players)
        {

            try
            {
                Debug.Write("Updating lobby for pin ", players[0].GamePin.ToString());
                await Clients.Group(players[0].GamePin.ToString()).CurrentLobby(players);
            }
            catch (NullReferenceException e)
            {
                await Task.FromException(e);
            }
        }

        /// <summary>
        /// Initiates new game round. 
        /// Let's the chameleon know he is chameleon.
        /// Sends question answer to everybody who is not chameleon.
        /// Sends question to all.
        /// </summary>
        /// 
        /// <param name="gamePin">
        /// Pin of current game
        /// </param>
        /// 
        /// <param name="skipInt">
        /// The (can be random) skip interval that context hops over to find a question
        /// </param>
        /// <param name="players">
        /// Players in Game
        /// </param>
        public async Task NewRound(string gamePin, int skipInt, List<Player> players) {
            var chameleonConnectionId = players[Random.Next(0, players.Count)].Id;

            var question = _mapper.Map<QuestionDTO>(await _context.Questions.Skip(skipInt).Include(q => q.Alternatives).FirstOrDefaultAsync());
            Debug.Write("Chameleon: " + chameleonConnectionId);
            await Clients.Client(chameleonConnectionId).AssignChameleon();
            var correctAnswer = question.Alternatives[Random.Next(0, question.Alternatives.Count)];
            await Clients.GroupExcept(gamePin, chameleonConnectionId).SecretAnswer(correctAnswer);

            await Clients.Group(gamePin).SendQuestion(question);
        }

        public async Task LeaveRoom(string roomName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, roomName);
        }



    }
}
