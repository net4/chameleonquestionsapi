﻿using ChameleonQuestionsAPI.DTOs;
using ChameleonQuestionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Hubs.Clients
{
    public interface IGameClient
    {
        public Task CurrentLobby(ICollection<Player> players);
        public Task JoinGame(Player player);

        public Task AssignChameleon();
        public Task SendQuestion(QuestionDTO question);
        public Task SecretAnswer(string answer);

    }
}
