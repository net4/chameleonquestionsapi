﻿using AutoMapper;
using ChameleonQuestionsAPI.DTOs;
using ChameleonQuestionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChameleonQuestionsAPI.Profiles
{
    public class QuestionProfile : Profile
    {
        public QuestionProfile() {
            CreateMap<Question, QuestionDTO>()
                .ForMember(dst => dst.Alternatives, src => src.MapFrom(q => q.Alternatives.Select(a => a.AlternativeString).ToArray()))
                .ReverseMap();
        }
    }
}
